package com.example.demo.group;

import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping(path = "/groups")
public class GroupController {

    GroupService groupService;

    public GroupController(GroupService groupService) {
        this.groupService = groupService;
    }

    @PostMapping("add_group")
    public GroupEntity addGroup(@RequestBody Group group) {
        return groupService.addGroupEntity(group);
    }

    @GetMapping("/get_group/{id}")
    public GroupEntity getById(@PathVariable("id") String id) {
        return groupService.getById(id);
    }
    @GetMapping("/get_all_groups")
    public List<GroupEntity> getAll(){
        return groupService.getAllGroups();
    }
    @PutMapping("/update_group_list/{id}/{groupId}")
    public GroupEntity addUserToGroup(@PathVariable("id") String id,
                                      @PathVariable("groupId") String groupId){
        return groupService.addUserToGroup(id, groupId);
    }
    @DeleteMapping("/delete_group/{id}")
    public List<GroupEntity> deleteGroupById(@PathVariable("id") String id){
        return groupService.deleteGroup(id);
    }
}
