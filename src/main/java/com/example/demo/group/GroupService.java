package com.example.demo.group;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class GroupService {

    public List<GroupEntity> groupEntities = new ArrayList<>();

    public GroupEntity addGroupEntity(Group group){
      GroupEntity groupEntity = new GroupEntity(UUID.randomUUID().toString(), group.getName(), group.getUsers());
      groupEntities.add(groupEntity);
      return groupEntity;
    }


    public GroupEntity getById(String id){
        GroupEntity groupEntity2 = new GroupEntity();
        for(GroupEntity groupEntity : groupEntities) {
            if(groupEntity.getId().equals(id)){
                groupEntity2 = groupEntity;
            }
        }
        return groupEntity2;
    }

    public GroupEntity addUserToGroup(String id, String groupId){
        GroupEntity groupEntity = getById(groupId);
        groupEntity.users.add(id);
        return groupEntity;
    }

    public List<GroupEntity> deleteGroup(String id){
        GroupEntity groupEntity = getById(id);
        groupEntities.remove(groupEntity);
        return groupEntities;
    }

    public List<GroupEntity> getAllGroups() {
        for(int i = 0; i < groupEntities.size(); i++){
            return groupEntities;
        }
        return null;
    }
}
