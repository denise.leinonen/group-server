package com.example.demo.group;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


import java.util.List;

@Data
@NoArgsConstructor
public class GroupEntity {
    String id;
    String name;
    List<String> users;

    @JsonCreator
    public GroupEntity(@JsonProperty ("id")String id,
                       @JsonProperty ("name") String name,
                       @JsonProperty ("users") List<String> users) {
        this.id = id;
        this.name = name;
        this.users = users;
    }
}
