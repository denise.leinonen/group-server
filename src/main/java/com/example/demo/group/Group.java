package com.example.demo.group;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Builder(toBuilder = true)
@Getter
@Setter
public class Group {
    String name;
    List<String> users;

    public Group(String name, List<String> users) {
        this.name = name;
        this.users = users;
    }
}
