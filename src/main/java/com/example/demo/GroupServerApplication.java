package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GroupServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(GroupServerApplication.class, args);
    }

}
